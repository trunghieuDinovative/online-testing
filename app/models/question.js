export class Question {
  //default value : Trong trường hợp không truyền tham số , thì sẽ lấy
  // giá trị mặc đinh
  constructor(questionType = "", _id = "", content = "", answers = []) {
    this.questionType = questionType;
    this._id = _id;
    this.content = content;
    this.answers = answers;
  }
}
