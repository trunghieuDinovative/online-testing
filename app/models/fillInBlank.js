import { Question } from "./question.js";
export class FillInBlank extends Question {
  constructor(...args) {
    super(...args);
  }
  //
  render(index) {
    return `
            <div>
                <h4>Câu hỏi ${index} : ${this.content} </h4>
                <input type="text"  />
            </div>
      `
  }
}
