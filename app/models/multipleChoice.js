import { Question } from "./question.js";

export class MultipleChoice extends Question {
  //rest parameters
  constructor(...args) {
    super(...args);
    //=>super(type,_id,content,answers)
  }
  renderAnswer() {
    let answersText = "";
    for (let ans of this.answers) {
      answersText += `
            <div>
                <input type="radio" name="${this._id}" />
                <span> ${ans.content} </span>
            </div>
        `;
    }
    return answersText;
  }
  render(index) {
    //   string template
    return `
            <div>
                <h4>Câu hỏi ${index} : ${this.content} </h4>
                ${this.renderAnswer()}
            </div>
      `;
  }
}

