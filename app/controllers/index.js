import { FillInBlank } from "../models/fillInBlank.js";
import { MultipleChoice } from "../models/multipleChoice.js";
import {Axios} from "../../node_modules/axios/index.js";
const questionList = [];
// lấy data từ DB
// arrow function
const getData = () => {
  Axios({
    method: "GET",
    url: "../../DeThiTracNghiem.json"
  })
    .then(res => {
      mapDataToObject(res.data);
      renderQuestion();
    })
    .catch(err => {
      console.log(err);
    });
};

const mapDataToObject = data => {
  for (let question of data) {
    //destructuring : bóc tách phần tử
    const { questionType, _id, content, answers } = question;
    // => const questionType = question.questionType

    let newQuestion = {};
    if (questionType === 1) {
      newQuestion = new MultipleChoice(questionType, _id, content, answers);
    } else {
      newQuestion = new FillInBlank(questionType, _id, content, answers);
    }
    questionList.push(newQuestion);
  }
};

const renderQuestion = () => {
  let content = "";
  for (let i in questionList) {
    content += questionList[i].render(i * 1 + 1);
  }
  console.log(content);
  document.getElementById("content").innerHTML = content;
};

getData();
// document.getElementById("btnSubmit").addEventListener("click", () => {
//   console.log(this);
// });
